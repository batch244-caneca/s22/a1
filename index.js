let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*Username registration*/

function register(username) {
    if (registeredUsers.includes(username)) {
        return alert("Registration failed. Username already exists!");
    } else {
        registeredUsers.push(username);
        return alert("Thank you for registering!");
    }
}

/*
    Browser console:
    register("Conan O' Brien");
    registeredUsers 
*/

/*Add a Registered User on friendslist*/

function addFriend(username) {
    if (friendsList.includes(username)) {
        return alert(`You have already added ${username} as a friend!`);
    } else if (registeredUsers.includes(username)) {
        friendsList.push(username);
        return alert(`You have added ${username} as a friend!`);
    } else {
        return alert("User not found.");
    }
}
/*
    Browser console:
    register("Conan O' Brien");
    registeredUsers 
    addFriend("Akiko Yukihime");
    addFriend("Conan O' Brien");
    addFriend("James Gunn");
*/

/*Display friends name on friendslist*/

function displayFriends() {
   let displayFriends = friendsList;
    if (friendsList.length === 0) {
        return alert("You currently have 0 friends. Add one first.");  
    } else {
        displayFriends.forEach(showFriends => {
            console.log(showFriends);
        })
    }
}
/*
    Browser console:
    displayFriends();
*/

/*Display number of friends on friendslist*/

function displayNumberOfFriends() {
    let displayNumberOfFriends = friendsList;
    if (friendsList.length >= 1) {
        return alert(`You currently have ${friendsList.length} friend/s.`);
    } else {
       return alert("You currently have 0 friends. Add one first."); 
    }
}
/*
    Browser console:
    displayNumberOfFriends();
*/

/*Delete a friend*/

function deleteFriend(index) {
    let enterIndex = friendsList.indexOf(index);
    let myFriendOnList = friendsList.splice(index, 1)
    if (friendsList.length === 0) {
        return alert("You currently have 0 friends. Add one first.");
    }
}
/*
    Browser console:
    deleteFriend([index#])
*/